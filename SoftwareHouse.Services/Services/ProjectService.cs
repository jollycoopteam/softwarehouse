﻿using SoftwareHouse.Contracts.Common;
using SoftwareHouse.Contracts.DataContracts;
using SoftwareHouse.Contracts.Repositories;
using SoftwareHouse.Contracts.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace SoftwareHouse.Services.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IProjectRepository _projectRepository;

        public ProjectService(IProjectRepository projectsRepository)
        {
            _projectRepository = projectsRepository;
        }

        public List<ProjectDto> GetAll()
        {
            return _projectRepository.GetAll();
        }

        public CommonResult<ProjectDto> GetById(int id)
        {
            var project = _projectRepository.GetById(id);

            if (project == null || project.IsDeleted)
            {
                return CommonResult<ProjectDto>.Failure<ProjectDto>("Problem occured during fetching project with given id.");
            }
            else
            {
                return CommonResult<ProjectDto>.Success<ProjectDto>(project);
            }
        }

        public CommonResult Add(AddProjectDto project)
        {
            if (string.IsNullOrEmpty(project.Name))
            {
                return CommonResult.Failure("Cannot create project without name provided.");
            }

            if (string.IsNullOrEmpty(project.Description))
            {
                return CommonResult.Failure("Cannot create project without description provided.");
            }

            var existingProject = _projectRepository.GetByName(project.Name);

            if (existingProject != null && !existingProject.IsDeleted && existingProject.Name == project.Name)
            {
                return CommonResult.Failure("Project name already exists.");
            }

            _projectRepository.Add(project);

            return CommonResult.Success();
        }

        public void Delete(int id)
        {
            _projectRepository.Delete(id);
        }
    }

}
