"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var React = require("react");
require("./ProjectsList.scss");
require("../../../Shared/Styles/helpers.scss");
var ProjectsListSummary_1 = require("../ProjectsListSummary/ProjectsListSummary");
var ProjectsListItem_1 = require("../ProjectsListItem/ProjectsListItem");
var ProjectsList = /** @class */ (function (_super) {
    __extends(ProjectsList, _super);
    function ProjectsList() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.paths = {
            createProjectUrl: '/Project/Add'
        };
        return _this;
    }
    ProjectsList.prototype.render = function () {
        return (React.createElement("section", { className: 'ProjectsList row' },
            React.createElement("div", { className: 'col-md-8 col-md-push-2' },
                React.createElement(ProjectsListSummary_1["default"], { projects: this.props.projects }),
                this.props.projects.map(function (project) {
                    return React.createElement(ProjectsListItem_1["default"], { key: project.id, project: project });
                }))));
    };
    return ProjectsList;
}(React.Component));
exports["default"] = ProjectsList;
