﻿export default class Project {
    id: number;
    name: string;
    description: string;
    isDeleted: boolean;
    creationDate: Date;
}