﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using SoftwareHouse.Contracts.Services;
using SoftwareHouse.Contracts.DataContracts;
using SoftwareHouse.Web.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SoftwareHouse.Web.Controllers
{
    [Authorize]
    public class ProjectController : Controller
    {

        private IProjectService _projectService;

        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View(new ProjectCreateViewModel());
        }

        [HttpPost]
        public IActionResult Add(ProjectCreateViewModel viewModel)
        {
            var result = _projectService.Add(new AddProjectDto
            {
                Name = viewModel.Name,
                Description = viewModel.Description
            });

            if (result.IsSuccess)
            {
                return RedirectToAction(nameof(ProjectController.Index), "Project");
            }
            else
            {
                viewModel.ErrorMessage = result.ErrorMessage;
                return View(viewModel);
            }
        }

        public IActionResult Details(int id)
        {
            var result = _projectService.GetById(id);

            return View(new ProjectViewModel()
            {
                Id = result.Item.Id,
                Name = result.Item.Name,
                Description = result.Item.Description,
                CreationDate = result.Item.CreationDate.Date.ToString("dd-MM-yyyy")
            });
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            _projectService.Delete(id);

            return RedirectToAction(nameof(ProjectController.Index), "Project");
        }
    }
}
