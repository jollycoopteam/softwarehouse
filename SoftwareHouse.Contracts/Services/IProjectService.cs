﻿using SoftwareHouse.Contracts.Common;
using SoftwareHouse.Contracts.DataContracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace SoftwareHouse.Contracts.Services
{
    public interface IProjectService
    {
        List<ProjectDto> GetAll();
        CommonResult<ProjectDto> GetById(int id);
        CommonResult Add(AddProjectDto project);
        void Delete(int id);
    }
}
