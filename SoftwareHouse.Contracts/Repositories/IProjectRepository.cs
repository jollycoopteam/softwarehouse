﻿using SoftwareHouse.Contracts.DataContracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace SoftwareHouse.Contracts.Repositories
{
    public interface IProjectRepository
    {
        List<ProjectDto> GetAll();
        ProjectDto GetById(int id);
        void Add(AddProjectDto project);
        ProjectDto GetByName(string projectName);
        void Delete(int id);
    }
}
